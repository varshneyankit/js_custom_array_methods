const items = require("../data");
const filter = require("../filter");

function callback(element) {
  if (element % 2 == 0) {
    return true;
  } else {
    return false;
  }
}

const filteredArray = filter(items, callback);
console.log(filteredArray);
