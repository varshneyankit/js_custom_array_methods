const items = require("../data");
const reduce = require("../reduce");

function callback(accumulator, currentValue) {
  return accumulator + currentValue;
}

const result = reduce(items, callback, 0);
console.log(result);
