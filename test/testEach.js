const items = require("../data");
const each = require("../each");

function callback(element, index) {
  console.log(element, index);
}

each(items, callback);
