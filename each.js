function each(elements, callback) {
  if (Array.isArray(elements) && typeof callback == "function") {
    for (let index = 0; index < elements.length; index++) {
      callback(elements[index], index);
    }
  } else {
    console.log("Data is incorrect");
  }
}

module.exports = each;
