function flatten(elements) {
  if (Array.isArray(elements)) {
    const flatArray = [];

    for (let element of elements) {
      if (Array.isArray(element)) {
        const nestedFlatArray = flatten(element);
        for (let nestedElement of nestedFlatArray) {
          flatArray.push(nestedElement);
        }
      } else {
        flatArray.push(element);
      }
    }

    return flatArray;
  } else {
    console.log("Data is incorrect");
    return [];
  }
}

module.exports = flatten;
