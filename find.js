function find(elements, callback) {
  if (Array.isArray(elements) && typeof callback == "function") {
    for (let element of elements) {
      let result = callback(element);
      if (result) {
        return element;
      }
    }

    return undefined;
  } else {
    console.log("Data is incorrect");
    return undefined;
  }
}

module.exports = find;
