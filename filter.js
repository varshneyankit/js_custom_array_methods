function filter(elements, callback) {
  if (Array.isArray(elements) && typeof callback == "function") {
    const filteredArray = [];

    for (let element of elements) {
      const value = callback(element);
      if (value) {
        filteredArray.push(element);
      }
    }

    return filteredArray;
  } else {
    console.log("Data is incorrect");
    return [];
  }
}

module.exports = filter;
