function map(elements, callback) {
  if (Array.isArray(elements) && typeof callback == "function") {
    const mappedArray = [];

    for (let element of elements) {
      const result = callback(element);
      mappedArray.push(result);
    }

    return mappedArray;
  } else {
    console.log("Data is incorrect");
    return [];
  }
}

module.exports = map;
