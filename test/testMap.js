const items = require("../data");
const map = require("../map");

function iteratee(element) {
  return element * 10;
}

function callback(element) {
  return iteratee(element);
}

const mappedArray = map(items, callback);
console.log(mappedArray);
