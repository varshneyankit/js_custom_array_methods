function reduce(elements, callback, startingValue) {
  if (Array.isArray(elements) && typeof callback == "function") {
    let accumulator = startingValue != undefined ? startingValue : elements[0];
    for (let element of elements) {
      accumulator = callback(accumulator, element);
    }
    return accumulator;
  } else {
    console.log("Data is incorrect");
    return undefined;
  }
}

module.exports = reduce;
